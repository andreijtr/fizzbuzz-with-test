package test.java;

import main.java.logic.FizzBuzz;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTest {

    @Test
    public void createStringTest() {

        FizzBuzz myFizz = new FizzBuzz(15);
        String[] expected = {"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"};
        String[] actualResult = myFizz.resolveProblem();

        Assert.assertArrayEquals(expected,actualResult);
    }
}

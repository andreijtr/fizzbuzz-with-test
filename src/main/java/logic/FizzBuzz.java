package main.java.logic;

import java.util.Arrays;

public class FizzBuzz {

    //This instance variable holds the number which will define the range for FizzBuzz display
    private int number;

    public FizzBuzz(int number) {
        this.number = number;
    }

    //This method returns an array of strings, based on given rules
    public String[] resolveProblem() {

        // Create a String[] array of size number (given number)
        String[] rangeToString = new String[number];

        //In this array we'll put all the numbers from range [1:number] including given number
        // and will apply the given rules
        for (int i = 1; i <= number; i++) {
            if (i % 15 == 0)
                rangeToString[i-1] = "FizzBuzz";
            else if (i % 3 == 0)
                rangeToString[i-1] = "Fizz";
            else if (i % 5 == 0)
                rangeToString[i-1] = "Buzz";
            else
                rangeToString[i-1] = i + "";
        }
        
        return rangeToString;
    }
}

package main.java.runner;

import main.java.logic.FizzBuzz;

import java.util.Arrays;

public class FizzBuzzRunner {

    public static void main(String[] args) {

        FizzBuzz myFizz = new FizzBuzz(100);

        System.out.println(Arrays.toString(myFizz.resolveProblem()));
    }

}
